﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{

    public struct Point3D
    {
        private int[] _coords;
        
        public Point3D(int x, int y, int z)
        {
            _coords = new int[4] { x, y, z, 1 };
        }

        public int X
        {
            get { return _coords[0]; }
            set { _coords[0] = value; }
        }
        public int Y
        {
            get { return _coords[1]; }
            set { _coords[1] = value; }
        }
        public int Z
        {
            get { return _coords[2]; }
            set { _coords[2] = value; }
        }
        public int[] Matrix
        {
            get { return _coords; }
        }
        public int Addition
        {
            get { return _coords[3]; }
            set { _coords[3] = value; }
        }
    }

    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            CFigure cube = new CFigure();
            cube.AddLine(new CLine(new Point3D(0, 0, 0), new Point3D(1, 1, 0)));
            cube.AddLine(new CLine(new Point3D(1, 1, 0), new Point3D(1, 1, 1)));
            cube.AddLine(new CLine(new Point3D(1, 1, 1), new Point3D(0, 0, 1)));
            cube.AddLine(new CLine(new Point3D(0, 0, 1), new Point3D(0, 0, 0)));
        }
    }
}
