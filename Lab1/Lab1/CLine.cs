﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public class CLine
    {
        protected Point3D _first;
        protected Point3D _second;
        public CLine(Point3D first, Point3D second)
        {
            _first = first;
            _second = second;
        }
        public Point3D First
        {
            get { return _first; }
            set { _first = value; }
        }
        public Point3D Second
        {
            get { return _second; }
            set { _second = value; }
        }
        public double Length
        {
            get
            {
                return Math.Sqrt((_second.X - _first.X) * (_second.X - _first.X)
                                 + (_second.Y - _first.Y) * (_second.Y - _first.Y)
                                 + (_second.Z - _first.Z) * (_second.Z - _first.Z));
            }
        }

    }
}
