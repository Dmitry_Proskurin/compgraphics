﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public class CDrawer
    {
        protected Graphics _graphic;
        protected Pen _pen;
        //смещения для рисования относительно центра экрана
        protected int _delta_x;
        protected int _delta_y;
        //
        protected Point _buff_point1, _buff_point2;
        public CDrawer(Graphics g, Pen p)
        {
            _graphic = g;
            _delta_x=(int)_graphic.VisibleClipBounds.Width/2;
            _delta_y=(int)_graphic.VisibleClipBounds.Height/2;
            _pen = p;
            _buff_point1 = new Point();
            _buff_point2 = new Point();
        }
        public Graphics Graphic
        {
            get { return _graphic; }
            set 
            {
                _graphic = value; 
                _delta_x=(int)_graphic.VisibleClipBounds.Width/2;
                _delta_y=(int)_graphic.VisibleClipBounds.Height/2;
            }
        }
        public void Draw(CLine line)
        {
            _buff_point1.X = line.First.X + _delta_x;
            _buff_point1.Y = line.First.Y + _delta_y;
            _buff_point2.X = line.Second.X + _delta_x;
            _buff_point2.Y = line.Second.Y + _delta_y;
            _graphic.DrawLine(_pen, _buff_point1, _buff_point2);
        }
        public void Draw(Point3D point)
        {
            _buff_point1.X = point.X + _delta_x;
            _buff_point1.Y = point.Y + _delta_y;
            throw new NotImplementedException();//лень реализовывать
            //можно рисовать эллипс, только нужно его заливать, высчитывать координаты левого верхнего угла 
            //и прочее... Кто это будет делать - не понятно, да и зачем? Всё равно рисуются только линии и фигуры
        }
        public void Draw(CFigure figure)
        {
            foreach (CLine line in figure.Lines)
            {
                Draw(line);
            }
        }
    }
}
