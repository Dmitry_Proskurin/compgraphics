﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public class CFigure
    {
        protected List<CLine> _lines;
        public CFigure()
        {
            _lines = new List<CLine>(5);
        }

        public void AddLine(CLine line)
        {
            _lines.Add(line);
        }

        public List<CLine> Lines
        {
            get { return _lines; }
        }

        public CLine this[int index]
        {
            get { return _lines[index]; }
            set { _lines[index] = value; }
        }
    }
}
