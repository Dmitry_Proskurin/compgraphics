﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class CTransformRotation:ITransformation
    {
        protected double[][] _matrix;
        protected int[] _result;//result for imuling of two matrix, make this for gc
        public CTransformRotation(int angle)
        {
            _result = new int[4];
            _matrix = new double[4][];
            foreach (int i in Enumerable.Range(0, 4))
            {
                _matrix[i] = new double[4];
            }
            _matrix[0][0] = _matrix[3][3] = 1;
            _matrix[1][1] = Math.Cos(angle);
            _matrix[1][2] = Math.Sin(angle);
            _matrix[2][1] = -_matrix[1][2];
            _matrix[2][2] = _matrix[1][1];
        }
        public double[][] Matrix
        {
            get { return _matrix; }
        }

        public void MakeTransform(Point3D point)
        {
            double summ = 0;
            int[] matrix=point.Matrix;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    summ += _matrix[i][j] * matrix[j];
                }
                _result[i] = (int)summ;
            }
            _result.CopyTo(matrix, 0);
        }

        public void MakeTransform(CLine line)
        {
            MakeTransform(line.First);
            MakeTransform(line.Second);
        }

        public void MakeTransform(CFigure figure)
        {
            foreach (CLine line in figure.Lines)
                MakeTransform(line);
        }
    }
}
