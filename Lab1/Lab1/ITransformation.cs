﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public interface ITransformation
    {
        double[][] Matrix{get;}
        void MakeTransform(Point3D point);
        void MakeTransform(CLine line);
        void MakeTransform(CFigure figure);
    }
}
